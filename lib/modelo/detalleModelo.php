<?php

@session_start();
date_default_timezone_set('America/Bogota');
include '../connection/connection.php';
include '../connection//BD.php';

class DetalleModel {

    public function listDetalleConcepto($data) {

        $texto = "";
        $obj_bd = new BD();

        $sql = "SELECT * FROM conceptos";

        $result = $obj_bd->EjecutaConsulta($sql);
        while ($array_data = $obj_bd->FuncionFetch($result)) {

            $texto .= '<option value="' . $array_data['idconcepto'] . '">' . htmlentities($array_data['descripcion']) . '</option>';
        }

        return $texto;
    }

    public function listPersonas($data) {

        $texto = "";
        $obj_bd = new BD();

        $sql = "SELECT * FROM personas WHERE activo=1";

        $texto = "<option value=''>-Seleccione-</option>";
        $result = $obj_bd->EjecutaConsulta($sql);
        while ($array_data = $obj_bd->FuncionFetch($result)) {

            $texto .= '<option value="' . $array_data['idpersona'] . '">' . htmlentities($array_data['nombres'] . " " . $array_data['apellidos']) . " - " . $array_data["nidentificacion"] . '</option>';
        }

        return $texto;
    }

    public function AddPersona($data) {

        $arreglo_general = array();

        $obj_bd = new BD();


        $sql = "SELECT * FROM personas where idpersona=" . $data['idPersona'] . " and activo=1";
        $result = $obj_bd->EjecutaConsulta($sql);
        $arreglo = $obj_bd->FuncionFetch($result);


        $arreglo_general['idpersona'] = $arreglo['idpersona'];
        $arreglo_general['fregistro'] = $arreglo['fregistro'];
        $arreglo_general['nombres'] = htmlentities($arreglo['nombres']);
        $arreglo_general['apellidos'] = htmlentities($arreglo['apellidos']);
        $arreglo_general['tidentificacion'] = $arreglo['tidentificacion'];
        $arreglo_general['nidentificacion'] = $arreglo['nidentificacion'];


        $json = json_encode($arreglo_general);
        return $json;
    }

    public function SaveDetalleConsecutivo($param) {

        $id_usuario = $_SESSION['Usuario']['ID_USUARIO'];

        $obj_bd = new BD();
        /**/


        /* data general */
        $Fechastart = filter_var(trim($param['Fechastart']), FILTER_SANITIZE_STRING);
        $sel_concepto = filter_var(trim($param['sel_concepto']), FILTER_SANITIZE_STRING);
        $txt_ingreso = filter_var(trim($param['txt_ingreso']), FILTER_SANITIZE_STRING);
        $txt_numero = filter_var(trim($param['txt_numero']), FILTER_SANITIZE_STRING);
        $txt_egreso = filter_var(trim($param['txt_egreso']), FILTER_SANITIZE_STRING);
        $txt_saldo = filter_var(trim($param['txt_saldo']), FILTER_SANITIZE_STRING);
        $observacion = htmlspecialchars(filter_var(trim($param['observacion']), FILTER_SANITIZE_STRING));
        /**/


        /* Datos de tabla personas */
        $arreglo_tb_personas = $param['tb_personas'];
        $arreglo_tb_fecha = $param['tb_fecha'];
        $arreglo_tb_valor = $param['tb_valor'];
        $arreglo_tb_Obs = $param['tb_Obs'];
        /**/



        /* Insertar detalle diario */

        $sql_insert = "INSERT INTO detalle_diario(  
                                                fecha,
                                                idconcepto,
                                                ingreso,
                                                cantidad,
                                                egreso,
                                                saldo,
                                                observacion,
                                                fecha_creacion,
                                                usuario_creacion
                                            )
                                            VALUES
                                            (      
                                                '" . $Fechastart . "',
                                                '" . $sel_concepto . "',
                                                '" . $txt_ingreso . "',
                                                '" . $txt_numero . "',
                                                '" . $txt_egreso . "',
                                                '" . $txt_saldo . "',
                                                '" . $observacion . "',
                                                now(),
                                                '" . $id_usuario . "'
                                                 
                                            )";
        $res_insert = $obj_bd->EjecutaConsulta($sql_insert);

        if ($res_insert != 1) {

            return 0; //Problemas ejecutando la query
        } else {

            /* validar si el concepto es diezmo */

            if ($sel_concepto == 2) {
                $iddetalle = $obj_bd->IdUltimoRegistro('iddetalle', 'detalle_diario');
                for ($indexPersona = 0; $indexPersona < count($arreglo_tb_personas); $indexPersona++) {

                    $sql_insert_personas = "INSERT INTO detalle_persona 
                                                                (
                                                                    iddetalle,
                                                                    Idperrsona,
                                                                    FechaRegistro,
                                                                    valor, 
                                                                    observacion, 
                                                                    FecharCreacion, 
                                                                    UsuarioCreo
                                                                ) 
                                                                VALUES 
                                                                (
                                                                    '" . $iddetalle . "',
                                                                    '" . filter_var(trim($arreglo_tb_personas[$indexPersona]), FILTER_SANITIZE_STRING) . "',
                                                                    '" . filter_var(trim($arreglo_tb_fecha[$indexPersona]), FILTER_SANITIZE_STRING) . "',
                                                                    '" . filter_var(trim($arreglo_tb_valor[$indexPersona]), FILTER_SANITIZE_STRING) . "',
                                                                    '" . htmlspecialchars(trim($arreglo_tb_Obs[$indexPersona]), FILTER_SANITIZE_STRING) . "',
                                                                    now(),
                                                                    '" . $id_usuario . "'                                                                    
                                                                )";

                    $res_insert_persona = $obj_bd->EjecutaConsulta($sql_insert_personas);
                    /**/
                    if ($res_insert_persona != 1) {
                        return 00; //Problemas ejecutando la query
                    }
                }
                if ($res_insert_persona != 1) {
                    return 00; //Problemas ejecutando la query
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }
    }

    function ListaDetalleDiario() {


        $obj_bd = new BD();
        $tabla = "";
        $boton_solicitud = '<button name="btnAgregarSolicitud" id="btnAgregarSolicitud" class="btn btn-warning" type="button" data-toggle="modal" data-target="#diezmosModal"> + Detalle Diario</button>';
        setlocale(LC_MONETARY, "es_ES");

        $tabla .= "<fieldset>";
        $tabla .= "<legend>Lista Detalle Diario</legend>";
        //  $tabla .= $boton_solicitud;
        $tabla .= "<br>";
        $tabla .= "<br>";
        $tabla .= '<div class="table-responsive">';
        $tabla .= '<table cellpadding="0" class="table table-bordered table-striped" cellspacing="0" border="0" id="example">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Concepto</th>
                            <th>Ingreso</th>
                            <th>Numero</th>
                            <th>Egreso</th>
                            <th>Saldo</th>
                            <th>Observación</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>';

        $sql = "select det_d.*, con.descripcion
                  from detalle_diario det_d, conceptos con
                 where det_d.idconcepto =con.idconcepto
                   and det_d.estado=1";
        $resultado = $obj_bd->EjecutaConsulta($sql);


        while ($row = $obj_bd->FuncionFetch($resultado)) {



            $btn = "<a  class='btn btn-danger btn-circle btn-sm' title='Eliminar Registro' onclick='EliminaDetalleDiario(" . $row['iddetalle'] . ");'><i class='fas fa-trash'></i></a>";
            $btn .= "<a  class='btn btn-success btn-circle btn-sm' onclick=''><i class='fas fa-check' title='Modificar Registro'></i></a>";


            $tabla .= "<tr>
 
                    <td>" . $row['fecha'] . "</td>
                    <td>" . $row['descripcion'] . "</td>
                    <td>" . number_format($row['ingreso'], 2, ',', '.') . "</td>
                    <td>" . $row['cantidad'] . "</td>
                    <td>" . number_format($row['egreso'], 2, ',', '.') . "</td>
                    <td>" . number_format($row['saldo'], 2, ',', '.') . "</td>
                    <td>" . $row['observacion'] . "</td>
                    
                    <td>" . $btn . "</td>
                </tr>";
        }

        $tabla .= "</tbody>
                    </table>
                    </div>
                    <script>$('#example').DataTable();</script>";
        $tabla .= "<fieldset>";
        return $tabla;
    }

    public function EliminaDetalleDiario($param) {

        $id_usuario = $_SESSION['Usuario']['ID_USUARIO'];

        $obj_bd = new BD();
        /**/


        /* data general */
        $iddetalle = filter_var(trim($param['iddetalle']), FILTER_SANITIZE_STRING);
        /**/


        /* Insertar detalle diario */

        $sql_update = "UPDATE detalle_diario SET estado = 0,
                                        fecha_modificacion = now(),
                                        usuario_modificacion = '" . $id_usuario . "'
                                  WHERE iddetalle = " . $iddetalle;
        $res_update = $obj_bd->EjecutaConsulta($sql_update);




        if ($res_update != '1') {

            return 0; //Problemas ejecutando la query
        } else {
            return 1;
        }
    }

}
