/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function listDetalleConcepto(control) {

    var retorno = "";

    $.ajax({
        type: "POST",
        url: "lib/controlador/detalleControlador.php",
        data: {
            opcion: 'listDetalleConcepto'

        },
        async: false,
        success: function (data) {

            retorno += data;

        }
    });
    $("#" + control + "").append(retorno);
}

function printAddPersona(id_select) {

    if (id_select == 2) {
        $("#add_personas").removeAttr("style");

    } else {
        $("#add_personas").css('display', 'none');
    }


}

function listPersonas(control) {

    var retorno = "";

    $.ajax({
        type: "POST",
        url: "lib/controlador/detalleControlador.php",
        data: {
            opcion: 'listPersonas'

        },
        async: false,
        success: function (data) {

            retorno += data;

        }
    });
    $("#" + control + "").append(retorno);
}


function AddPersona() {

    var idPersona = $("#sel_personas").val();
    var json;


    var valor_acumulado = $("#val_acumulado").html();


    if (idPersona == "") {
        alert("Por favor seleccione la persona que desea agregar. ");
        return false;
    } else {

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: "lib/controlador/detalleControlador.php",
            data: {
                opcion: 'AddPersona',
                idPersona: idPersona
            },
            async: false,
            success: function (response) {
                json = response;
            }
        });
        // return json;
        if ($("#tr_" + json.idpersona).val() == null)
        {

            table_per = "<tr id='tr_" + json.idpersona + "'>" +
                    "<td><a  class='btn btn-danger btn-circle btn-sm'  onclick='eliminartr(" + json.idpersona + ");'><i class='fas fa-trash'></i></a>" +
                    "<td>" + json.nombres + " " + json.apellidos + "</td>" +
                    "<td><input class='form-control' type='date' id='id_fecha_" + json.idpersona + "' name='trip-start' placeholder='AAAA-MM-DD'  min='2020-01-01'>  </td>" +
                    "<td><input class='form-control' type='number' id='id_valor_" + json.idpersona + "' onchange='ValidarAcumulado(" + json.idpersona + ");'>  </td>" +
                    "<td>  <textarea id='tax_" + json.idpersona + "' class='form-control' placeholder='Detalle'></textarea> </td>" +
                    '<input type="hidden" name="idPersona[]" id="idPersona[]" value="' + json.idpersona + '">' +
                    "</tr>";

            $("#tr_add_more_personas").append(table_per);
            $("#div_tbl_personas").removeAttr("style");

        } else {

            alert('Señor usuario este registro ya esta agregado.');
            return false;

        }

    }

}

function formatoPesos(valor) {

    const formatterPeso = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP',
        minimumFractionDigits: 0
    });
    valor = formatterPeso.format(valor);
    return valor;
}


function ValidarAcumulado(id_persona) {


    //  var valor_acumulado_mostrar = parseInt($("#val_acumulado").html());
    var valor_acumulado = parseInt($("#val_acumulado_sum").html());
    var valor_reg = parseInt($("#id_valor_" + id_persona).val());

    valor_acumulado = parseInt(valor_acumulado + valor_reg);
    $("#val_acumulado").html(formatoPesos(valor_acumulado));
    $("#val_acumulado_sum").html(valor_acumulado);
    $("#txt_saldo").val(valor_acumulado);
    $("#txt_ingreso").val(valor_acumulado);
    $("#id_valor_" + id_persona + "").attr('disabled', 'disabled');


}

function eliminartr(inicial) {
    var valor_acumulado = $("#val_acumulado_sum").html();
    var valor_reg = parseInt($("#id_valor_" + inicial).val());

    var confrimar = confirm("Esta seguro que desea eliminar este registro");

    if (confrimar) {

        valor_acumulado = valor_acumulado - valor_reg;


        $("#val_acumulado").html(formatoPesos(valor_acumulado));
        $("#val_acumulado_sum").html(valor_acumulado);
        $("#txt_saldo").val(valor_acumulado);
        $("#txt_ingreso").val(valor_acumulado);
        $("#tr_" + inicial).remove();
        $("#id_valor_" + inicial).removeAttr("disabled");


    }
}

function contadorFilasTablaGeneral(id1, id2) {
    var numFilas = $('#' + id1 + ' >tr').length;
    if (numFilas === 0) {
        $("#" + id2 + "").val('');
    } else {
        $("#" + id2 + "").val(numFilas);
    }
}

function SaveDetalleConsecutivo() {

    var valida_formulario = true;
    var arregloPersonasDiezmo = new Array();
    var arregloFecha = new Array();
    var arregloValor = new Array();
    var arregloObs = new Array();
    var tb_personas = "";
    var tb_fecha = "";
    var tb_valor = "";
    var tb_Obs = "";
    var total_tb = $("#val_acumulado_sum").html();

    var Fechastart = $("#Fechastart").val();
    var sel_concepto = $("#sel_concepto").val();
    var txt_ingreso = $("#txt_ingreso").val();
    var txt_numero = $("#txt_numero").val();
    var txt_egreso = $("#txt_egreso").val();
    var txt_saldo = $("#txt_saldo").val();
    var observacion = $("#observacion").val();


    if (sel_concepto == 2) {
        var txt_ctl_personas = $("#txt_ctl_personas").val();
        if (txt_ctl_personas == '') {
            valida_formulario = false;
            $("#msg_add_personas").html('Por favor agregar por lo menos una persona.');
        } else {
            $("#msg_add_personas").html('');
            $('input[name^="idPersona"]').each(function () {
                arregloPersonasDiezmo.push($(this).val());

                arregloFecha.push($("#id_fecha_" + $(this).val()).val());
                arregloValor.push($("#id_valor_" + $(this).val()).val());
                arregloObs.push($("#tax_" + $(this).val()).val());

            });
            tb_personas = arregloPersonasDiezmo;
            tb_fecha = arregloFecha;
            tb_valor = arregloValor;
            tb_Obs = arregloObs;

            ;
        }
    }

    if (sel_concepto == "") {
        valida_formulario = false;
        $("#msg_concepto").html('Por favor seleccione el concepto');
    } else {
        $("#msg_concepto").html('');
    }
    if (Fechastart == "") {
        valida_formulario = false;
        $("#msg_fecha").html('Por favor ingrese la fecha');
    } else {
        $("#msg_fecha").html('');
    }


    if (valida_formulario == true) {
        $.ajax({
            type: 'POST',
            url: "lib/controlador/detalleControlador.php",
            data: {
                opcion: 'SaveDetalleConsecutivo',
                Fechastart: Fechastart,
                sel_concepto: sel_concepto,
                txt_ingreso: txt_ingreso,
                txt_numero: txt_numero,
                txt_egreso: txt_egreso,
                txt_saldo: txt_saldo,
                observacion: observacion,
                tb_personas: tb_personas,
                tb_fecha: tb_fecha,
                tb_valor: tb_valor,
                tb_Obs: tb_Obs

            },
            async: false,
            success: function (response) {
                if (response == 1) {
                    alert("Señor usuario, los datos han sido guardados exitosamente");
                    limpiarFormularioDetalle();
                    ListaDetalleDiario();
                } else {
                    alert("Señor usuario, los datos no pudieron registrarse");
                }
            }
        });

    } else {
        alert('Hay información en el formulario sin diligenciar por favor completarla!');
        return false;
    }
}

function limpiarFormularioDetalle() {

    $("#Fechastart").val('');

    $("#txt_ingreso").val('');
    $("#txt_numero").val('');
    $("#txt_egreso").val('');
    $("#txt_saldo").val('');
    $("#observacion").val('');
    $('#sel_concepto').val('');
    $('#sel_personas').select2().val('').trigger('change');

    $("#tr_add_more_personas").html('');
    $("#val_acumulado_sum").html(0);
    $("#val_acumulado").html(0);
    $("#add_personas").css('display', 'none');
}

function ListaDetalleDiario() {
    $.ajax({
        type: "POST",
        url: "lib/controlador/detalleControlador.php",
        async: false,
        data: {
            opcion: 'ListaDetalleDiario'
        },
        success: function (retu) {
            $("#contenido").html(retu);
        }
    });
}

function EliminaDetalleDiario(iddetalle) {
    var confirmacion = confirm('Señor usuario, ¿Esta seguro de eliminar el detalle Diario?');
    
    if (confirmacion == true) {
        $.ajax({
            type: "POST",
              url: "lib/controlador/detalleControlador.php",
            data: {
                opcion: 'EliminaDetalleDiario',
                iddetalle:iddetalle
            },
            async: false,
            success: function (response) {
                if (response == 1) {
                    alert('Señor usuario, el registro se elimino correctamente.');
                    ListaDetalleDiario();
                } else {
                    alert("Señor usuario, se presento un error, por favor intentar mas tarde.");
                }
            }
        });
    } else {
        return false;
    }

}