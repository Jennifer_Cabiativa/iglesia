CREATE DATABASE idiospen;

CREATE TABLE idiospen.conceptos  
   (idconcepto int PRIMARY KEY NOT NULL,  
   descripcion varchar(25) NOT NULL,  
   tipo varchar(25) COMMENT 'Ingreso / Egreso' NOT NULL);
   
CREATE TABLE idiospen.perfiles  
   (idperfil int PRIMARY KEY NOT NULL,  
   descripcion varchar(25) NOT NULL,
   fecha_creacion date NOT NULL,
   usuario_creacion varchar(25) NOT NULL,
   fecha_modificacion date,
   usuario_modificacion varchar(25)
   );
   
CREATE TABLE idiospen.personas  
   (idpersona int PRIMARY KEY NOT NULL,  
   fregistro date NOT NULL,
   nombres varchar(25) NOT NULL,
   apellidos varchar(25) NOT NULL,
   tidentificacion int COMMENT '1: Cédula de Ciudadanía / 2: Cédula de Extranjería / 3: Pasaporte / 4: Registro Civil / 5: Tarjeta de Identidad' NOT NULL,
   nidentificacion int NOT NULL,
   fnacimiento date,
   bautizado int COMMENT '1: Si / 0: No',
   fbautizo date,
   contacto int,
   contacto2 int,
   lider int COMMENT '1: Si / 0: No',
   area int COMMENT '1: Alabanza / 2: Caballeros / 3: Damas / 4: Diaconado / 5: Esc. Biblica / 6: Evangelización / 7: Interseción / 8: Jovenes / 9: Junta Local / 10: Parejas',
   activo int COMMENT '1: Si / 0: No',
   fretiro date,
   factivacion date,
   idperfil int,
   usuario varchar(25),
   clave varchar(25),
   usuactivo int COMMENT '1: Activo / 0: Inactivo',
   fecha_creacion date NOT NULL,
   usuario_creacion varchar(25) NOT NULL,
   fecha_modificacion date,
   usuario_modificacion varchar(25),
   FOREIGN KEY (idperfil) REFERENCES perfiles(idperfil)
   );
   
CREATE TABLE idiospen.detalle_diario  
   (iddetalle int PRIMARY KEY NOT NULL,  
   fecha date NOT NULL,  
   idconcepto int NOT NULL,
   idpersona int,
   protemplo int COMMENT '1: Si / 0: No', 
   observacion_diezmo varchar(25),
   ingreso int,
   cantidad int,
   egreso int,
   saldo int,
   observacion varchar(25),
   cierre int COMMENT '1: Si / 0: No',
   fcierre date,
   fecha_creacion date NOT NULL,
   usuario_creacion varchar(25) NOT NULL,
   fecha_modificacion date,
   usuario_modificacion varchar(25),
   FOREIGN KEY (idconcepto) REFERENCES conceptos(idconcepto),
   FOREIGN KEY (idpersona) REFERENCES personas(idpersona)
   );
   
   CREATE TABLE idiospen.proyectos  
   (idproyecto int PRIMARY KEY NOT NULL,  
   idconcepto int NOT NULL,  
   idpersona int,
   fecha date NOT NULL,
   ingreso int,
   cantidad int,
   egreso int,
   saldo int,
   observaciones varchar(25),
   fecha_creacion date NOT NULL,
   usuario_creacion varchar(25) NOT NULL,
   fecha_modificacion date,
   usuario_modificacion varchar(25),
   FOREIGN KEY (idconcepto) REFERENCES conceptos(idconcepto),
   FOREIGN KEY (idpersona) REFERENCES personas(idpersona)
   );
    
    insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (1,"Ofrendas","ingreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (2,"Diezmos","ingreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (3,"Pago recibo Internet","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (4,"Pago recibo Telefono","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (5,"Pago recibo Agua","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (6,"Pago recibo Luz","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (7,"Pago recibo Gas","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (8,"Salario Pastoral","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (9,"Aseo","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (10,"Cartelera","ingreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (11,"Otros egresos","egreso");
	insert into idiospen.conceptos (idconcepto, descripcion, tipo) values (12,"Otros ingresos","ingreso");
