-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: idiospen
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `conceptos`
--

DROP TABLE IF EXISTS `conceptos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conceptos` (
  `idconcepto` int NOT NULL,
  `descripcion` varchar(25) NOT NULL,
  `tipo` varchar(25) NOT NULL COMMENT 'Ingreso / Egreso',
  PRIMARY KEY (`idconcepto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conceptos`
--

LOCK TABLES `conceptos` WRITE;
/*!40000 ALTER TABLE `conceptos` DISABLE KEYS */;
INSERT INTO `conceptos` VALUES (1,'Ofrendas','ingreso'),(2,'Diezmos','ingreso'),(3,'Pago recibo Internet','egreso'),(4,'Pago recibo Telefono','egreso'),(5,'Pago recibo Agua','egreso'),(6,'Pago recibo Luz','egreso'),(7,'Pago recibo Gas','egreso'),(8,'Salario Pastoral','egreso'),(9,'Aseo','egreso'),(10,'Cartelera','ingreso'),(11,'Otros egresos','egreso'),(12,'Otros ingresos','ingreso');
/*!40000 ALTER TABLE `conceptos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_diario`
--

DROP TABLE IF EXISTS `detalle_diario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalle_diario` (
  `iddetalle` int NOT NULL,
  `fecha` date NOT NULL,
  `idconcepto` int NOT NULL,
  `idpersona` int DEFAULT NULL,
  `protemplo` int DEFAULT NULL COMMENT '1: Si / 0: No',
  `observacion_diezmo` varchar(25) DEFAULT NULL,
  `ingreso` int DEFAULT NULL,
  `cantidad` int DEFAULT NULL,
  `egreso` int DEFAULT NULL,
  `saldo` int DEFAULT NULL,
  `observacion` varchar(25) DEFAULT NULL,
  `cierre` int DEFAULT NULL COMMENT '1: Si / 0: No',
  `fcierre` date DEFAULT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`iddetalle`),
  KEY `idconcepto` (`idconcepto`),
  KEY `idpersona` (`idpersona`),
  CONSTRAINT `detalle_diario_ibfk_1` FOREIGN KEY (`idconcepto`) REFERENCES `conceptos` (`idconcepto`),
  CONSTRAINT `detalle_diario_ibfk_2` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`idpersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_diario`
--

LOCK TABLES `detalle_diario` WRITE;
/*!40000 ALTER TABLE `detalle_diario` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_diario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `perfiles` (
  `idperfil` int NOT NULL,
  `descripcion` varchar(25) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idperfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfiles`
--

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personas` (
  `idpersona` int NOT NULL,
  `fregistro` date NOT NULL,
  `nombres` varchar(25) NOT NULL,
  `apellidos` varchar(25) NOT NULL,
  `tidentificacion` int NOT NULL COMMENT '1: Cédula de Ciudadanía / 2: Cédula de Extranjería / 3: Pasaporte / 4: Registro Civil / 5: Tarjeta de Identidad',
  `nidentificacion` int NOT NULL,
  `fnacimiento` date DEFAULT NULL,
  `bautizado` int DEFAULT NULL COMMENT '1: Si / 0: No',
  `fbautizo` date DEFAULT NULL,
  `contacto` int DEFAULT NULL,
  `contacto2` int DEFAULT NULL,
  `lider` int DEFAULT NULL COMMENT '1: Si / 0: No',
  `area` int DEFAULT NULL COMMENT '1: Alabanza / 2: Caballeros / 3: Damas / 4: Diaconado / 5: Esc. Biblica / 6: Evangelización / 7: Interseción / 8: Jovenes / 9: Junta Local / 10: Parejas',
  `activo` int DEFAULT NULL COMMENT '1: Si / 0: No',
  `fretiro` date DEFAULT NULL,
  `factivacion` date DEFAULT NULL,
  `idperfil` int DEFAULT NULL,
  `usuario` varchar(25) DEFAULT NULL,
  `clave` varchar(25) DEFAULT NULL,
  `usuactivo` int DEFAULT NULL COMMENT '1: Activo / 0: Inactivo',
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idpersona`),
  KEY `idperfil` (`idperfil`),
  CONSTRAINT `personas_ibfk_1` FOREIGN KEY (`idperfil`) REFERENCES `perfiles` (`idperfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyectos` (
  `idproyecto` int NOT NULL,
  `idconcepto` int NOT NULL,
  `idpersona` int DEFAULT NULL,
  `fecha` date NOT NULL,
  `ingreso` int DEFAULT NULL,
  `cantidad` int DEFAULT NULL,
  `egreso` int DEFAULT NULL,
  `saldo` int DEFAULT NULL,
  `observaciones` varchar(25) DEFAULT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idproyecto`),
  KEY `idconcepto` (`idconcepto`),
  KEY `idpersona` (`idpersona`),
  CONSTRAINT `proyectos_ibfk_1` FOREIGN KEY (`idconcepto`) REFERENCES `conceptos` (`idconcepto`),
  CONSTRAINT `proyectos_ibfk_2` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`idpersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyectos`
--

LOCK TABLES `proyectos` WRITE;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-02 10:53:08
