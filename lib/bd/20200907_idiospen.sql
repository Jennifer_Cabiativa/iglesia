-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: idiospen
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `conceptos`
--

DROP TABLE IF EXISTS `conceptos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conceptos` (
  `idconcepto` int(11) NOT NULL,
  `descripcion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(25) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Ingreso / Egreso',
  PRIMARY KEY (`idconcepto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conceptos`
--

LOCK TABLES `conceptos` WRITE;
/*!40000 ALTER TABLE `conceptos` DISABLE KEYS */;
INSERT INTO `conceptos` VALUES (1,'Ofrendas','ingreso'),(2,'Diezmos','ingreso'),(3,'Pago recibo Internet','egreso'),(4,'Pago recibo Telefono','egreso'),(5,'Pago recibo Agua','egreso'),(6,'Pago recibo Luz','egreso'),(7,'Pago recibo Gas','egreso'),(8,'Salario Pastoral','egreso'),(9,'Aseo','egreso'),(10,'Cartelera','ingreso'),(11,'Otros egresos','egreso'),(12,'Otros ingresos','ingreso');
/*!40000 ALTER TABLE `conceptos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_diario`
--

DROP TABLE IF EXISTS `detalle_diario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalle_diario` (
  `iddetalle` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `idconcepto` int(11) NOT NULL,
  `protemplo` int(11) DEFAULT NULL COMMENT '1: Si / 0: No',
  `ingreso` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `egreso` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `observacion` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cierre` int(11) DEFAULT NULL COMMENT '1: Si / 0: No',
  `fcierre` date DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario_creacion` varchar(25) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'CURRENT_TIMESTAMP',
  `fecha_modificacion` datetime DEFAULT NULL,
  `usuario_modificacion` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` varchar(45) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1: Activo, 0:Eliminado',
  PRIMARY KEY (`iddetalle`),
  KEY `idconcepto` (`idconcepto`),
  CONSTRAINT `detalle_diario_ibfk_1` FOREIGN KEY (`idconcepto`) REFERENCES `conceptos` (`idconcepto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_diario`
--

LOCK TABLES `detalle_diario` WRITE;
/*!40000 ALTER TABLE `detalle_diario` DISABLE KEYS */;
INSERT INTO `detalle_diario` VALUES (1,'2020-09-08',2,NULL,12000,2,0,12000,'algó',NULL,NULL,'2020-09-07 16:22:41','',NULL,NULL,'1'),(2,'2020-09-08',1,NULL,20000,1,0,20000,'validar',NULL,NULL,'2020-09-07 16:23:45','',NULL,NULL,'1'),(3,'2020-09-08',3,NULL,0,1,100000,0,'pago validar texto',NULL,NULL,'2020-09-07 17:19:07','','2020-09-07 17:49:53','','1');
/*!40000 ALTER TABLE `detalle_diario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_persona`
--

DROP TABLE IF EXISTS `detalle_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detalle_persona` (
  `IdDetallePersona` int(11) NOT NULL AUTO_INCREMENT,
  `iddetalle` int(11) NOT NULL,
  `Idperrsona` int(11) NOT NULL,
  `FechaRegistro` date DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `observacion` varchar(4000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Estado` varchar(45) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1' COMMENT '1:Activo, 0:Eliminado',
  `FecharCreacion` timestamp NULL DEFAULT current_timestamp(),
  `UsuarioCreo` int(11) NOT NULL,
  PRIMARY KEY (`IdDetallePersona`),
  KEY `Idperrsona` (`Idperrsona`),
  KEY `iddetalle` (`iddetalle`),
  CONSTRAINT `Idperrsona` FOREIGN KEY (`Idperrsona`) REFERENCES `personas` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iddetalle` FOREIGN KEY (`iddetalle`) REFERENCES `detalle_diario` (`iddetalle`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_persona`
--

LOCK TABLES `detalle_persona` WRITE;
/*!40000 ALTER TABLE `detalle_persona` DISABLE KEYS */;
INSERT INTO `detalle_persona` VALUES (1,1,1,'2020-09-07',2000,'ok','1','2020-09-07 21:22:41',0),(2,1,2,'2020-09-09',10000,'actividád','1','2020-09-07 21:22:41',0);
/*!40000 ALTER TABLE `detalle_persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `perfiles` (
  `idperfil` int(11) NOT NULL,
  `descripcion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idperfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfiles`
--

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personas` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `fregistro` date NOT NULL,
  `nombres` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `tidentificacion` int(11) NOT NULL COMMENT '1: Cédula de Ciudadanía / 2: Cédula de Extranjería / 3: Pasaporte / 4: Registro Civil / 5: Tarjeta de Identidad',
  `nidentificacion` int(11) NOT NULL,
  `fnacimiento` date DEFAULT NULL,
  `bautizado` int(11) DEFAULT NULL COMMENT '1: Si / 0: No',
  `fbautizo` date DEFAULT NULL,
  `contacto` int(11) DEFAULT NULL,
  `contacto2` int(11) DEFAULT NULL,
  `lider` int(11) DEFAULT NULL COMMENT '1: Si / 0: No',
  `area` int(11) DEFAULT NULL COMMENT '1: Alabanza / 2: Caballeros / 3: Damas / 4: Diaconado / 5: Esc. Biblica / 6: Evangelización / 7: Interseción / 8: Jovenes / 9: Junta Local / 10: Parejas',
  `activo` int(11) DEFAULT 1 COMMENT '1: Si / 0: No',
  `fretiro` date DEFAULT NULL,
  `factivacion` date DEFAULT NULL,
  `idperfil` int(11) DEFAULT NULL,
  `usuario` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuactivo` int(11) DEFAULT NULL COMMENT '1: Activo / 0: Inactivo',
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idpersona`),
  KEY `idperfil` (`idperfil`),
  CONSTRAINT `personas_ibfk_1` FOREIGN KEY (`idperfil`) REFERENCES `perfiles` (`idperfil`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,'0000-00-00','Maria','Lopez',0,123456789,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','',NULL,NULL),(2,'0000-00-00','prueba','sistema',0,987654321,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00','',NULL,NULL);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proyectos` (
  `idproyecto` int(11) NOT NULL,
  `idconcepto` int(11) NOT NULL,
  `idpersona` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `ingreso` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `egreso` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `observaciones` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_creacion` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modificacion` date DEFAULT NULL,
  `usuario_modificacion` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idproyecto`),
  KEY `idconcepto` (`idconcepto`),
  KEY `idpersona` (`idpersona`),
  CONSTRAINT `proyectos_ibfk_1` FOREIGN KEY (`idconcepto`) REFERENCES `conceptos` (`idconcepto`),
  CONSTRAINT `proyectos_ibfk_2` FOREIGN KEY (`idpersona`) REFERENCES `personas` (`idpersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyectos`
--

LOCK TABLES `proyectos` WRITE;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-07 19:07:33
