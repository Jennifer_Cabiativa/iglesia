<?php


include '../modelo/detalleModelo.php';
$opcion = filter_var(trim($_POST['opcion']), FILTER_SANITIZE_STRING);
$obj_detalle = new DetalleModel();

if ($opcion == 'listDetalleConcepto') {

    (string) $resultado = $obj_detalle->listDetalleConcepto($_POST);
    echo $resultado;
}
if ($opcion == 'listPersonas') {

    (string) $resultado = $obj_detalle->listPersonas($_POST);
    echo $resultado;
}

if ($opcion == 'AddPersona') {
    $retorno = $obj_detalle->AddPersona($_POST);
    echo $retorno;
}

if ($opcion == 'SaveDetalleConsecutivo') {
    $resultado = $obj_detalle->SaveDetalleConsecutivo($_POST);
    echo $resultado;
}
if ($opcion == 'ListaDetalleDiario') {
    $resultado = $obj_detalle->ListaDetalleDiario($_POST);
    echo $resultado;
}
if ($opcion == 'EliminaDetalleDiario') {
    $resultado = $obj_detalle->EliminaDetalleDiario($_POST);
    echo $resultado;
}