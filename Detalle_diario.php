<!DOCTYPE html>
<html lang="en">

    <head>


        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">




    </head>
    <style>
        .estiloError {
            font-weight: bold;
            color: red;
            padding: 2px 8px;
            margin-top: 2px;
        }
    </style>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-fw fa-wrench"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">Administrador<sup></sup></div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Tablero</span></a>
                </li>

                <!-- Nav Item - Pages Collapse Menu --> 
                <li class="nav-item">
                    <a class="nav-link" href="Detalle_diario.php">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Detalle Diario</span></a>
                </li>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Cierres</span>
                    </a>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="buttons.html">Cierre Mensual</a>
                            <a class="collapse-item" href="cards.html">Cierre Anual</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Entradas
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link" href="ofrendas.html">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>Ofrendas</span></a>
                </li>

                <!-- Nav Item - Utilities Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link" href="diezmos.html">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Diezmos</span></a>
                </li>
                <!-- Nav Item - Utilities Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                        <i class="fas fa-fw fa-wrench"></i>
                        <span>Pro-templo</span>
                    </a>
                    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="Proyectos.html">Proyectos</a>
                            <a class="collapse-item" href="pro-templo.html">Entrada</a>
                            <a class="collapse-item" href="utilities-animation.html">Salida</a>
                            <a class="collapse-item" href="utilities-other.html">Other</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Salidas
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="gastos.html">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Gastos</span></a>
                </li>

                <!-- Nav Item - Charts -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Inversiones</span>
                    </a>
                </li>
                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Organización
                </div>

                <!-- Nav Item - Tables -->
                <li class="nav-item">
                    <a class="nav-link" href="tables.html">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Miembros</span></a>
                </li>

                <!-- Nav Item - Tables -->
                <li class="nav-item">
                    <a class="nav-link" href="cronograma.html">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Cronograma</span></a>
                </li>

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-50">
                            <img class="img-profile rounded-circle" src="https://www.facebook.com/IDiospenSanFrancisco/photos/a.100381898281845/100381808281854">
                            <h1 class="text-xs font-weight-bold text-info text-uppercase mb-1">Iglesia de Dios Pentecostal San Francisco</h1>
                        </div>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                            <li class="nav-item dropdown no-arrow d-sm-none">
                                <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-search fa-fw"></i>
                                </a>
                                <!-- Dropdown - Messages -->
                                <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto w-100 navbar-search">
                                        <div class="input-group">
                                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="button">
                                                    <i class="fas fa-search fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">Alejandra Pachon Rico</span>
                                    <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Perfil
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Cerrar sesión
                                    </a>
                                </div>
                            </li>

                        </ul>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <!-- Page Heading -->
                        <table class="table border" id="dataTable" width="100%" cellspacing="0">
                            <tbody><tr>
                                    <td colspan="5"></td>
                                    <td><div class="input-group">
                                            <div class="d-sm-flex align-items-baseline justify-content-between mb-4">
                                                <h1 class="small font-weight-bold"></h1>
                                                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-warning shadow-sm" data-toggle="modal" data-target="#diezmosModal"><i class="fas fa-fw fa-sm text-white-50"></i> + Detalle Diario</a>
                                            </div>

                                        </div></td>
                                    <td><div class="d-sm-flex align-items-center justify-content-between mb-4">
                                            <h1 class="small font-weight-bold"></h1>
                                            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generar Reporte</a>
                                        </div></td>
                                </tr></tbody>
                        </table>


                        <!-- Content Row contenido de la tabla detalle diario -->

                        <div class="row">
<!--                                                        <a href="#" class="btn btn-warning btn-circle btn-sm" data-toggle="modal" data-target="#diezmosModal">
                                                            <i class="fas fa-plus"></i>
                                                        </a>-->
                            <div class="col-xs-12 col-sm-12 col-md-12">      
                                <div id="contenido"></div>      
                            </div>
                        </div>

                        <!-- /.container-fluid -->

                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                    <footer class="sticky-footer bg-white">
                        <div class="container my-auto">
                            <div class="copyright text-center my-auto">
                                <span>Copyright &copy; @Idiospen San Francisco 2020</span>
                            </div>
                        </div>
                    </footer>
                    <!-- End of Footer -->

                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->

            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>

            <!-- Logout Modal-->
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Listo para salir?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Seleccione "Cerrar sesión" si desea finalizar su sesión actual.</div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" data-dismiss="modal">Cancelar</button>
                            <a class="btn btn-primary" href="login.html">Cerrar sesión</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Delete Modal-->
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body text-center">¿Esta seguro de eliminar el registro?</div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" data-dismiss="modal">Aceptar</button>
                            <a class="btn btn-primary" href="">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Success Modal-->
            <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body text-center"><i class="fas fa-check"></i> Registro guardado satisfactoriamente.</div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" type="button" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Diezmos Modal -->
            <div class="modal fade" id="diezmosModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Detalle Diario</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body"> <!-- Inicia Body -->
                            <div class="container">
                                <div class="form-group" id="Fecha_det">
                                    <label for="txt_detalle">Fecha</label>
                                    <input class="form-control" type="date" id="Fechastart" name="trip-start" placeholder="AAAA-MM-DD"  min="2020-01-01" max="2020-12-31">  
                                    <p id="msg_fecha" class="estiloError" style="text-align: center;"></p>
                                </div>

                                <div class="form-group" >
                                    <label for="txt_concepto">Concepto</label>
                                    <select name="sel_concepto" id="sel_concepto" class="form-control" onchange="printAddPersona(this.value)">
                                        <option value="">- Seleccione -</option>    
                                    </select>   
                                    <p id="msg_concepto" class="estiloError" style="text-align: center;"></p>
                                </div>

                                <div id="add_personas" style="display:none">
                                    <div class="form-group" ></div>
                                    <center><h6>Agregar Personas</h6></center>
                                    <div class="form-group">
                                        <select name="sel_personas" id="sel_personas"  style="width: 100%" class="form-control"  ></select> 

                                    </div><div class="input-group-append">
                                        <button class="btn btn-primary" type="button" onclick="AddPersona();contadorFilasTablaGeneral('tr_add_more_personas', 'txt_ctl_personas');">
                                            <i class="fas fa-plus fa-sm"></i>
                                        </button>
                                    </div>
                                    <div class="row">


                                    </div>

                                    <table>
                                        <tr rowspan="2">
                                            <td colspan="2">
                                                <div id="div_tbl_personas" style="display: none;">                        
                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <td>Acción</td>
                                                                <td>Nombre</td>
                                                                <td>Fecha</td>
                                                                <td>Valor</td>
                                                                <td>Descripcion</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tr_add_more_personas">
                                                        </tbody>
                                                    </table>
                                                    <table class="table table-bordered table-striped">
                                                        <tr>  <td id="val_acumulado_sum" class="lead" style="display: none">0</td>
                                                            <td colspan="3" align="right"><b>Total: $</b></td>
                                                            <td id="val_acumulado" class="lead">0</td>

                                                        </tr>
                                                    </table>
                                                    <input type="hidden" name="txt_ctl_personas" id="txt_ctl_personas" value="">

                                                </div>
                                                <p id="msg_add_personas" class="estiloError" style="text-align: center;"></p>
                                            </td>
                                        </tr>
                                    </table>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row"></div>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group" >
                                    <label for="txt_concepto">Ingreso</label>
                                    <input type="number" id="txt_ingreso" placeholder="0" class="form-control" >
                                </div>

                                <div class="form-group" >
                                    <label for="txt_concepto">Numero </label>
                                    <input type="number" id="txt_numero" placeholder="0" class="form-control" >
                                </div>

                                <div class="form-group" >
                                    <label for="txt_concepto">Egreso </label>
                                    <input type="number" id="txt_egreso" placeholder="0" class="form-control" >
                                </div>

                                <div class="form-group" >
                                    <label for="txt_concepto">Saldo </label>
                                    <input type="number" id="txt_saldo" placeholder="0" class="form-control" >
                                </div>

                                <div class="form-group" >
                                    <label for="txt_concepto">Observación </label>
                                    <textarea  id="observacion" class="form-control" placeholder="Sobrecosto en el recibo del mes"></textarea>

                                </div>

                            </div>  <!-- Fin Body -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="limpiarFormularioDetalle();">Salir</button>
                                <button type="button" class="btn btn-primary" onclick="SaveDetalleConsecutivo();">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div></div>

            <!-- CRUP -->

            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/jquery/jquery-2.1.4.js" type="text/javascript"></script>

            <!-- Bootstrap core JavaScript-->
            <script src="vendor/jquery-ui-1.11.4.custom/jquery-ui.js" type="text/javascript"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!--            <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
            <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">-->
            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin-2.min.js"></script>

            <!-- Page level plugins -->
            <script src="vendor/datatables/jquery.dataTables.min.js"></script>
            <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

            <link href="vendor//select2/select2.min.css" rel="stylesheet" />
            <link href="vendor//select2/select2-bootstrap.css" rel="stylesheet" />
            <script src="vendor//select2/select2.min.js"></script>

            <script src="lib/js/detalle_concepto.js" type="text/javascript"></script>


    </body>

</html>

<script>
                                    ListaDetalleDiario();
                                    $('#sel_personas').select2();
                                    $(document).ready(function () {
                                        listDetalleConcepto("sel_concepto");
                                        listPersonas("sel_personas");

                                    });
</script>